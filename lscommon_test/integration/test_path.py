import json
import unittest

from lscommon.location.path import Path
from lscommon.location.point import Point, points_from_csv


class PathTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @staticmethod
    def load_sugarloaf_lifts():
        with open("lscommon_test/integration/data/sugarloaf_lifts.json") as f:
            lifts = json.loads(f.read()).get("lifts")
        for lift in lifts:
            nodes = lift.get("nodes")
            nodes = [tuple(node) for node in nodes]
            lift["nodes"] = nodes
        return lifts

    @staticmethod
    def _load_andrew_suglarloaf():
        return points_from_csv("lscommon_test/integration/data/andrew_sugarloaf.csv")

    def test_correct_lift(self):
        lifts = PathTest.load_sugarloaf_lifts()
        lift = lifts[0]
        lat, long = lift.get("nodes")[0]
        assert lift == Path.closest_lift(lifts, Point(lat, long, 0., 0.))

    def test_update_point(self):
        points = PathTest._load_andrew_suglarloaf()
        path = Path(lifts=PathTest.load_sugarloaf_lifts())
        for point in points:
            path.update_position(point)
        assert False
