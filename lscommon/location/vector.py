from dataclasses import dataclass
from math import sqrt

from lscommon.location.point import Point


@dataclass
class Vector:
    lat: float
    long: float
    alt: float
    time: float

    def __repr__(self):
        return f"<Vector ({self.lat}, {self.long}, {self.alt}, {self.time}s)>"

    def __str__(self):
        return repr(self)

    def normalize(self):
        magnitude = sqrt(self.lat**2 + self.lat**2 + self.alt**2)
        self.lat = self.lat / magnitude
        self.long = self.long / magnitude
        self.alt = self.alt / magnitude

    @staticmethod
    def from_points(old: Point, new: Point):
        """Create a vector from two point."""
        return Vector(
            new.lat - old.lat,
            new.long - old.long,
            new.alt - old.alt,
            new.time - old.time,
        )

    def from_lift(self):
        pass

