from dataclasses import dataclass
from typing import List


@dataclass
class Point:
    lat: float
    long: float
    alt: float
    time: float


def points_from_csv(file_path) -> List[Point]:
    with open(file_path, 'r') as f:
        lines = f.readlines()
    points = []
    for line in lines[1:]:
        lat, long, alt, time = line.split(',')
        points.append(Point(float(lat), float(long), float(alt), float(time.strip())))
    return points
