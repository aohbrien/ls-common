"""
    Path is very distinctly *not* a track, or a ski track for that matter.
"""
from geopy.distance import geodesic

from lscommon.location.point import Point
from lscommon.location.vector import Vector


class Path(object):
    DERIVATIVE_THRESHOLD_UPHILL = 0.4
    DERIVATIVE_THRESHOLD_DOWNHILL = -2
    ALTITUDE_THRESHOLD_UPHILL = 100
    ALTITUDE_THRESHOLD_DOWNHILL = 50

    def __init__(self, lifts=None):
        self.prev: Point = None
        self.ridden_lifts = []
        self.riding_lift = False
        self.start_elev: float = 0.
        self.hill_lifts = lifts

    def update_position(self, point: Point):
        """
        Update a new point to the path
        :param point: new point
        """
        if not self.prev:
            self.start_elev = point.alt
            self.prev = point
            return
        riding_lift, skiing = self.skiing_or_lift(point)

        # Update state / determine lift or trail
        if not self.riding_lift and riding_lift:
            self.ridden_lifts.append(point.time)
            self.riding_lift = True
            print("Lift now: ", Path.closest_lift(self.hill_lifts, point).get("name"))
        elif self.riding_lift and skiing:
            print("SKIING NOW")
            self.riding_lift = False
            self.start_elev = point.alt
        # Update previous point
        self.prev = point

    def skiing_or_lift(self, point):
        # Calculate delta & derivative
        alt_derivative = self.derivative(point)
        alt_delta = abs(point.alt - self.start_elev)
        # Determine if riding lift or skiing
        riding_lift = alt_derivative >= Path.DERIVATIVE_THRESHOLD_UPHILL and \
                      alt_delta >= Path.ALTITUDE_THRESHOLD_UPHILL
        skiing = alt_derivative < Path.DERIVATIVE_THRESHOLD_UPHILL and \
                 alt_delta >= Path.ALTITUDE_THRESHOLD_DOWNHILL
        return riding_lift, skiing

    def derivative(self, point):
        if point.time == self.prev.time:
            alt_derivative = 0
        else:
            alt_derivative = (point.alt - self.prev.alt) / (point.time - self.prev.time)
        return alt_derivative

    @staticmethod
    def closest_lift(lifts, point):
        if not lifts:
            return None
        lift_node_distances = [[geodesic(node, (point.lat, point.long)) for node in lift.get("nodes")] for lift in
                               lifts]
        closest_node = [min([node for node in nodes]) for nodes in lift_node_distances]
        return lifts[closest_node.index(min(closest_node))]
